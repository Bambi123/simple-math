# 3x + 1

print("Please enter a number to begin: ")
x = int(input())


while (x != 1):
    print(f"Number: {x}")
    if (x % 2 == 0):
        x = int(x/2)
    else:
        x = (3 * x + 1)